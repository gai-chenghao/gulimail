package com.atguigu.gulimall.order.config;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.atguigu.gulimall.order.vo.PayVo;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "alipay")
@Component
@Data
public class AlipayTemplate {

    //在支付宝创建的应用的id
    private   String app_id = "2021000119622386";

    // 商户私钥，您的PKCS8格式RSA2私钥
    private  String merchant_private_key = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQC4pQxpdWYyxUI3kvIWBun+pjhyTr3203/L0Etcx7UyNV8gUuCk0pGl2J1l5HdUuEK1Yic2+8aDagxwWRlLx2Y+8JEUYoiowLdqkkM9RvSEHHyXH3p6R3257M5GgJ7ZEidG6pjtbbB4qbPPcJu/LIwyt27rvxboYvA8v5hnlz8Esy/Lw4yU1rqcbe56Ka7ZsHIsh6KOCL21VvqgzW9bW+d10QtgvxMQAKlvxa9+3ydPGcszcXztvxhZZN7j82Sj8IYocqVyDK2L28ONdxZnvgDnCX5doTnG3UNXeN9lnJPEjZbPSHw9/YErkDeKMfqJ/0fs7XFBWMnsFGHq5I2Yg3tlAgMBAAECggEAJhSD9svrg9UCzwtTOg+6+nW1dn2e1+TOmm4e3MejKvOu5q+w/M0glJ+T9oujwKkvuNDBXamXS0r3vcPiX6qD/78FbLrVjFgSe7wmeeDEVOq7Fz+6trU/zLR15jxohkQkLY/8chsJJhl5p/KFbGv3jg4epigGxW6CNUUeXV7n9EPWghq0G4rS66yeVNjEPUn7VEeN6uxMLHUgyyWgvTJqxq0IhzRMDCfE6vGr2aL7SuNygk0YQbzZ1HmiEtCGLsYPsu6EnNTTG7LyPPumW/3iT/WQ5U8hMVgeY8see7I5LaTHbAJLf0rBRGdsZlG63pcm8OWT05zyl1y+FdqptA3JkQKBgQDgiUsOAnvFsw6Pi2zlTkQL+ihh4Pkda/S6X3tLypRo3yvYCeEa8JudIgxKmXK4TA11k5KpIqLM0doJZX8ZGcOJyQrvyU/nbUSOk4dG2kHE8gBly+1rXJ89QYnPwsLyPn+L+5BfgwyFWmoUgzDu/bQCSXpBZzJc6KRrcoYaeAJAywKBgQDShL1R+yeLGber4QqmbYf3AtCNErEXmPwtAaKGTkazEiQd+EzmKJC+ENaV2gFwpKwqe5aAeiz4i337XdPP6MxEQIco1GzFM09/1KBF5ll91dLo76WXZxx3WgPtpDNRER3uoO0USmRb0s164Nr9c0EXRWtepoqeBo5kRqek7UyejwKBgFmRivYguX0Iy3MpfBwQ14YrmTZKRzWcXfiGpCl8uy0GcXfxoPFdL8ZD0cFkauXnuKrBXozHu0eXIrIC4euCYoUxRIKYvJ4l8WYqpZj28at9mfkBm0jrnr4Rycat+5QkqD6P8GyZW0JK6ewa1UdQvyD2bKVwZB+50fTmr2YZn1R3AoGAdLZe98qUe8wARmx3qRlkng+TbAMG55xLgvA7GNbg33qZR3xXteqbhKR1M5Mv5TGGue7W29fggtNjPy8ftU7NoLFdvcc3noT5+Fb7nIU+7ELVsYrbdXsXDxx6vQ+h27nvrFLP3cX4qefPP+bVwlpZpfs4sbsHcuETpIh/fTKKngUCgYBRv1P9L9o+8Qf8mlAQ8yJVA8C/DlgkGRplN1Zbz5D3RFO8KF3CNVVAE3LIHgnzC4csySw8/+hwZBHbQ6jnf+whFGKukWRzcU3JvIs5ZU3F5weyQTnAhB+FFriiO/OaheJ0pEfF4LankVhqXJ1ay+DdOs8rzm9fOvqB7Lhz8k2ZiA==";
    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    private  String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArzswepOSX8tww2pIRVJ8+hlVaPx+/lzD8+StQMtfhZoQX4AnR0XMx9p5+XASfmxqdZSYgo1aoVtSkRbJgeQhp9EobQ6PGrpJMuHg3t8ChB1iyh1poAQX9jiP6k+Uv7brMAgOHG9vWprAMvMediG3PaowLr0OWGyQkzot/mth+O5c4sIplo5OyhLX6JQ9CtQPRqFLn99txupOzezQLh2+joGPlw4WgnKURtf3jQZKH9BLFo85Ovvq2cZhjLyoRih6pN5lXjZFd/pTrxL5Xl09v/YUtrc7eIsClrWyn8iSzyzQV1lofIyzoLgkvVRxxBj/Pi8UbIiw+xMcGAYOqVYxDQIDAQAB";
    // 服务器[异步通知]页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    // 支付宝会悄悄的给我们发送一个请求，告诉我们支付成功的信息
    private  String notify_url="http://7huon1qrjk.51xd.pub/payed/notify";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    //同步通知，支付成功，一般跳转到成功页
    //private  String return_url="http://7huon1qrjk.51xd.pub/alipay.trade.page.pay-JAVA-UTF-8/return_url.jsp";

    private  String return_url="http://member.gulimall.com/memberOrder.html";
    // 签名方式
    private  String sign_type = "RSA2";

    // 字符编码格式
    private  String charset = "utf-8";

    // 支付宝网关； https://openapi.alipaydev.com/gateway.do
    private  String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    public  String pay(PayVo vo) throws AlipayApiException {

        //AlipayClient alipayClient = new DefaultAlipayClient(AlipayTemplate.gatewayUrl, AlipayTemplate.app_id, AlipayTemplate.merchant_private_key, "json", AlipayTemplate.charset, AlipayTemplate.alipay_public_key, AlipayTemplate.sign_type);
        //1、根据支付宝的配置生成一个支付客户端
        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrl,
                app_id, merchant_private_key, "json",
                charset, alipay_public_key, sign_type);

        //2、创建一个支付请求 //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(return_url);
        alipayRequest.setNotifyUrl(notify_url);

        //商户订单号，商户网站订单系统中唯一订单号，必填
        String out_trade_no = vo.getOut_trade_no();
        //付款金额，必填
        String total_amount = vo.getTotal_amount();
        //订单名称，必填
        String subject = vo.getSubject();
        //商品描述，可空
        String body = vo.getBody();

        alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
                + "\"total_amount\":\""+ total_amount +"\","
                + "\"subject\":\""+ subject +"\","
                + "\"body\":\""+ body +"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        String result = alipayClient.pageExecute(alipayRequest).getBody();

        //会收到支付宝的响应，响应的是一个页面，只要浏览器显示这个页面，就会自动来到支付宝的收银台页面
        System.out.println("支付宝的响应："+result);

        return result;

    }
}

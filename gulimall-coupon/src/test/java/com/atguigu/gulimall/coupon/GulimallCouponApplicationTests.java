package com.atguigu.gulimall.coupon;

import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

//@SpringBootTest
class GulimallCouponApplicationTests {

    public static void main(String[] args) {
        // 2022-02-25 00:00:00 2022-02-27 23:59:59
        LocalDate now = LocalDate.now();
        LocalDate plusDays = now.plusDays(1);
        LocalDate plusDays1 = now.plusDays(2);

        LocalTime min = LocalTime.MIN;
        LocalTime max = LocalTime.MAX;

        System.out.println(min);
        System.out.println(max);

        LocalDateTime of = LocalDateTime.of(now, min);
        LocalDateTime of1 = LocalDateTime.of(plusDays1, max);
        System.out.println(of);
        System.out.println(of1);;

    }

    @Test
    void contextLoads() {
        LocalDate now = LocalDate.now();
        LocalDate plusDays = now.plusDays(1);
        LocalDate plusDays1 = now.plusDays(2);

        LocalTime min = LocalTime.MIN;
        LocalTime max = LocalTime.MAX;

        System.out.println(min);
        System.out.println(max);


    }

}

package com.atguigu.gulimall.seckill.service.impl;

import com.alibaba.csp.sentinel.Entry;
import com.alibaba.csp.sentinel.SphU;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.atguigu.common.to.mq.SeckillOrderTo;
import com.atguigu.common.utils.R;
import com.atguigu.common.vo.MemberRespVo;
import com.atguigu.gulimall.seckill.feign.CouponFeignService;
import com.atguigu.gulimall.seckill.feign.ProductFeignService;
import com.atguigu.gulimall.seckill.interceptor.LoginUserInterceptor;
import com.atguigu.gulimall.seckill.service.SeckillService;
import com.atguigu.gulimall.seckill.to.SecKillSkuRedisTo;
import com.atguigu.gulimall.seckill.vo.SeckillSessionWithSkus;
import com.atguigu.gulimall.seckill.vo.SkuInfoVo;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.redisson.api.RSemaphore;
import org.redisson.api.RedissonClient;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SeckillServiceImpl implements SeckillService {

    @Autowired
    CouponFeignService couponFeignService;

    @Autowired
    StringRedisTemplate redisTemplate;

    @Autowired
    ProductFeignService productFeignService;


    @Autowired
    RedissonClient redissonClient;

    @Autowired
    RabbitTemplate rabbitTemplate;

    private final String SESSIONS_CACHE_PREFIX="seckill:sessions";

    private final String SKUKILL_CACHE_PREFIX="seckill:skus";

    private final String SKU_STOCK_SEMAPHORE="seckill:stock:";//+随机码


    @Override
    public void uploadSeckillSkuLatest3Days() {
        //1 扫描最近三天需要参与秒杀的活动
        R session = couponFeignService.getLates3DaySession();
        if(session.getCode()==0){
            //上架商品
            List<SeckillSessionWithSkus> sessionWithSkus = session.getData(new TypeReference<List<SeckillSessionWithSkus>>() {
            });
            //缓存到redis
            //1\ 缓存活动信息
            saveSessionInfos(sessionWithSkus);
            //2 缓存活动的关联商品信息
            saveSessionSkuInfos(sessionWithSkus);
        }
    }

    /**
     * blockHandler 函数会在原方法被限流/降级/系统保护的时候调用 而fallback函数会针对所有类型的异常
     * @param e
     * @return
     */
    public List<SecKillSkuRedisTo> blockHandler(BlockException e){
        log.info("异常"+e.getMessage());
        return null;
    }

    //返回当前时间可以参与的秒杀商品信息
    @SentinelResource(value = "getCurrentSeckillSkusResurce",blockHandler = "blockHandler")
    @Override
    public List<SecKillSkuRedisTo> getCurrentSeckillSkus() {
        //1 确定当前时间属于那个秒杀场次
        //1970
        long time = new Date().getTime();
        try (Entry seckillSkus = SphU.entry("seckillSkus")){
            Set<String> keys = redisTemplate.keys(SESSIONS_CACHE_PREFIX + "*");
            for (String key : keys) {
                String replace = key.replace(SESSIONS_CACHE_PREFIX, "");
                String[] s = replace.split("_");
                long start = Long.parseLong(s[0]);
                long end = Long.parseLong(s[1]);
                if(time>=start && time<=end){
                    //2 获取这个秒杀场次需要的所有商品信息
                    List<String> range = redisTemplate.opsForList().range(key, -100, 100);
                    BoundHashOperations<String, String, String> hashOps = redisTemplate.boundHashOps(SKUKILL_CACHE_PREFIX);
                    List<String> list = hashOps.multiGet(range);
                    if(list!=null){
                        List<SecKillSkuRedisTo> collect = list.stream().map(item -> {
                            SecKillSkuRedisTo redis = JSON.parseObject((String) item, SecKillSkuRedisTo.class);
                            //redis.setRandomCode(null); 当前秒杀开始就需要随机码
                            return redis;
                        }).collect(Collectors.toList());
                        return collect;
                    }
                    break;
                }
            }
        }catch (BlockException e){
            log.error("资源被限流,{}",e.getMessage());
        }




        return null;
    }

    @Override
    public SecKillSkuRedisTo getSkuSeckillInfo(Long skuId) {
        //1\ 找到所有需要参与秒杀的商品的key

        BoundHashOperations<String, String, String> hashOps = redisTemplate.boundHashOps(SKUKILL_CACHE_PREFIX);

        Set<String> keys = hashOps.keys();
        if(keys!=null && keys.size()>0){
            String regx = "\\d_" + skuId;
            for (String key : keys) {
                //6_4
                if(Pattern.matches(regx,key)){
                    String json = hashOps.get(key);
                    SecKillSkuRedisTo to = JSON.parseObject(json, SecKillSkuRedisTo.class);

                    //随机码
                    long current = new Date().getTime();
                    if(current>=to.getStartTime() && current<=to.getEndTime()){

                    }else{
                        to.setRandomCode(null);
                    }
                    return to;
                }
            }
        }
        return null;
    }

    // TODO: 2022/2/28 上架秒杀商品的时候 每一个数据都有过期时间 
    // TODO: 2022/2/28 秒杀后续的流程 简化了收货地址等信息
    @Override
    public String kill(String killId, String key, Integer num) {


        long l1 = System.currentTimeMillis();

        MemberRespVo memberRespVo = LoginUserInterceptor.loginUser.get();


        // 1 获取当前秒杀商品的详细信息

        BoundHashOperations<String, String, String> hashOps = redisTemplate.boundHashOps(SKUKILL_CACHE_PREFIX);

        String json = hashOps.get(killId);
        if(StringUtils.isEmpty(json)){
            return null;
        }else {
            SecKillSkuRedisTo redis = JSON.parseObject(json, SecKillSkuRedisTo.class);
            //校验合法性
            Long startTime = redis.getStartTime();
            Long endTime = redis.getEndTime();
            long time = new Date().getTime();
            long ttl = endTime - time;
            //1\校验时间的合法行
            if(time>=startTime && time<=endTime){
                //2 校验随机吗 和商品id
                String randomCode = redis.getRandomCode();
                String skuid = redis.getPromotionSessionId() + "_" + redis.getSkuId();
                if(randomCode.equals(key) && killId.equals(skuid)){
                    //3\ 验证购物数量 是否合理
                    if(num<=redis.getSeckillLimit().intValue()){
                        //4\验证这个人是否已经购买过 幂等性 如果致癌药秒杀成功 就去占位 userId_session_skuId
                        //SETNX
                        String redisKey = memberRespVo.getId() + "_" + skuid;
                        //自动过期
                        Boolean aBoolean = redisTemplate.opsForValue().setIfAbsent(redisKey, num.toString(),ttl,TimeUnit.MILLISECONDS);
                        if(aBoolean){
                            //占位成功说明从来没有买过

                            RSemaphore semaphore = redissonClient.getSemaphore(SKU_STOCK_SEMAPHORE + randomCode);

                                //110
                                //boolean b = semaphore.tryAcquire(num,100, TimeUnit.MILLISECONDS);
                                boolean b = semaphore.tryAcquire(num);
                                if(b){
                                    //秒杀成功
                                    //快速下单 发送mq消息 10ms

                                    String timeId = IdWorker.getTimeId();

                                    SeckillOrderTo orderTo=new SeckillOrderTo();
                                    orderTo.setOrderSn(timeId);
                                    orderTo.setMemberId(memberRespVo.getId());
                                    orderTo.setNum(num);
                                    orderTo.setPromotionSessionId(redis.getPromotionSessionId());
                                    orderTo.setSkuId(redis.getSkuId());
                                    orderTo.setSeckillPrice(redis.getSeckillPrice());


                                    rabbitTemplate.convertAndSend("order-event-exchange","order.seckill.order",orderTo);

                                    long l2 = System.currentTimeMillis();
                                    log.info("耗时...{}",(l2-l1));
                                    return timeId;
                                }else {
                                    return null;
                                }


                        }else {
                            //说明已经买过了
                            return  null;
                        }


                    }
                }else {
                    return null;
                }
            }


        }

        return null;
    }


    private  void saveSessionInfos(List<SeckillSessionWithSkus> sessions){

        sessions.stream().forEach(session->{
            Long startTime = session.getStartTime().getTime();
            Long endtime = session.getEndTime().getTime();
            String key=SESSIONS_CACHE_PREFIX+startTime+"_"+endtime;
            Boolean aBoolean = redisTemplate.hasKey(key);
            if(!aBoolean){
                List<String> collect = session.getRelationSkus().stream().map(item ->item.getPromotionSessionId()+"_"+ item.getSkuId().toString()).collect(Collectors.toList());
                //缓存活动信息
                redisTemplate.opsForList().leftPushAll(key,collect);
            }

        });

    }

    private void saveSessionSkuInfos(List<SeckillSessionWithSkus> sessions){

        sessions.stream().forEach(session -> {
            //准备hash操作
            BoundHashOperations<String, Object, Object> hashOps = redisTemplate.boundHashOps(SKUKILL_CACHE_PREFIX);
            session.getRelationSkus().stream().forEach(seckillSkuVo -> {
                //4 随机码？
                String randomCode=UUID.randomUUID().toString().replace("-","");

                if(!hashOps.hasKey(seckillSkuVo.getPromotionSessionId().toString()+"_"+seckillSkuVo.getSkuId().toString())){
                    //缓存商品
                    SecKillSkuRedisTo redisTo = new SecKillSkuRedisTo();
                    //1\sku的基本数据
                    R skuInfo = productFeignService.getSkuInfo(seckillSkuVo.getSkuId());
                    if(skuInfo.getCode()==0){
                        SkuInfoVo info = skuInfo.getData("skuInfo", new TypeReference<SkuInfoVo>() {
                        });
                        redisTo.setSkuInfoVo(info);
                    }


                    //2 sku的秒杀信息
                    BeanUtils.copyProperties(seckillSkuVo,redisTo);

                    //3 设置上当前商品的秒杀时间信息

                    redisTo.setStartTime(session.getStartTime().getTime());
                    redisTo.setEndTime(session.getEndTime().getTime());


                    redisTo.setRandomCode(randomCode);
                    String s= JSON.toJSONString(redisTo);
                    hashOps.put(seckillSkuVo.getPromotionSessionId().toString()+"_"+seckillSkuVo.getSkuId().toString(),s);
                    //如果当前这个场次的商品的库存信息已经上架就不需要上架
                    //5 使用库存作为分布式的信号量 限流：
                    RSemaphore semaphore = redissonClient.getSemaphore(SKU_STOCK_SEMAPHORE + randomCode);
                    //商品可以秒杀的数量作为信号
                    semaphore.trySetPermits(seckillSkuVo.getSeckillCount().intValue());
                }
            });
        });

    }


}

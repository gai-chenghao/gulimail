package com.atguigu.gulimall.member.interceptor;

import com.atguigu.common.constant.AuthServerConstant;
import com.atguigu.common.vo.MemberRespVo;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class LoginUserInterceptor implements HandlerInterceptor {


    public static ThreadLocal<MemberRespVo> loginUser=new ThreadLocal<>();
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        //member/memberreceiveaddress/info/{id}
        // /order/order/status/231231321
        //feign远程调用放行
        String uri = request.getRequestURI();
        boolean match = new AntPathMatcher().match("/member/**", uri);
        System.out.println("是否匹配"+match);
        if(match){
            return true;
        }


        MemberRespVo attribute = (MemberRespVo) request.getSession().getAttribute(AuthServerConstant.LOGIN_USER);
        if(attribute!=null){

            loginUser.set(attribute);
            return true;

        }else {
            //没登录就去登陆
            request.getSession().setAttribute("msg","请先进行登陆");
            response.sendRedirect("http://auth.gulimall.com/login.html");
            return false;
        }


    }
}

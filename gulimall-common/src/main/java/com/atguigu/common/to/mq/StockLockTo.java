package com.atguigu.common.to.mq;

import lombok.Data;

@Data
public class StockLockTo {

    private Long id;//库存工作单的id
    private StockDetailTo detail;//工作详情的所哟id
}
